import axios from 'axios';

export const FETCH_CONTACTS_REQUEST = 'FETCH_CONTACTS_REQUEST';
export const FETCH_CONTACTS_SUCCESS = 'FETCH_CONTACTS_SUCCESS';
export const FETCH_CONTACTS_FAILURE = 'FETCH_CONTACTS_FAILURE';
export const POST_CONTACT = "POST_CONTACT";

export const FETCH_INFO = 'FETCH_INFO';

export const fetchContactsRequest = () => ({type: FETCH_CONTACTS_REQUEST});
export const fetchContactsSuccess = books => ({type: FETCH_CONTACTS_SUCCESS, books});
export const fetchContactsFailure = () => ({type: FETCH_CONTACTS_FAILURE});
export const fetchInfoSuccess = cont => ({type: FETCH_INFO, cont});

export const fetchContacts = () => {
    return async (dispatch) => {
        try {
            dispatch(fetchContactsRequest());
            const response = await axios.get('https://exam9-89c3c-default-rtdb.firebaseio.com/contacts.json');
            const books = Object.keys(response.data).map(id => ({
                ...response.data[id],
                id
            }));
            dispatch (fetchContactsSuccess(books));
        } catch (error) {
            dispatch (fetchContactsFailure());
        }
    };
};

export const deleteContact = id => {
    return async dispatch => {
        try {
            await axios.delete('https://exam9-89c3c-default-rtdb.firebaseio.com/contacts/' + id + '.json');
            dispatch(fetchContacts());
        } catch (error) {
            console.log(error);
        }
    };
};

export const newContactAdd= (input, props) => {
    return async dispatch => {
        try {
            await axios.post ('https://exam9-89c3c-default-rtdb.firebaseio.com/contacts.json', input);
            dispatch(fetchContactsSuccess());
        } finally {
            props.history.push('/')
        }
    }
};


export const putNewContact = (id, data) => {
    return async dispatch => {
        try {
            await axios.put('https://exam9-89c3c-default-rtdb.firebaseio.com/contacts/' + id + '.json', data);
            dispatch(fetchContacts());
        } catch (e) {
            console.log(e);
        }
    }
};

export const fetchInfoContact = (id) => {
    return async dispatch => {
        try{
            dispatch(fetchContactsRequest());
            const response = await axios.get('https://exam9-89c3c-default-rtdb.firebaseio.com/contacts/' + id +'.json');
            const cont = (response.data);
            dispatch(fetchInfoSuccess(cont));
        } catch (error) {
            dispatch(fetchContactsFailure(error));
        }
    }
};