import {
    FETCH_DISH_SUCCESS,
    FETCH_DISHES_FAILURE,
    FETCH_DISHES_REQUEST,
    FETCH_DISHES_SUCCESS,
    POST_DISHES
} from "../action/actionDishes";

const initialState = {
    loading: false,
    dishes: [],
    dish: [],
    error: false,
};

const reducerDishes = (state = initialState, action) => {
    switch (action.type) {
        case POST_DISHES:
            return {...state, dishes: action.input};
        case FETCH_DISHES_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_DISHES_SUCCESS:
            return {...state, dishes: action.dishes, fetchLoading: false};
        case FETCH_DISHES_FAILURE:
            return {...state, fetchLoading: false, fetchError: action.error};
        case FETCH_DISH_SUCCESS:
            return {...state, dish: action.dish, fetchLoading: false};
        default:
            return state;
    }
    return state;
};

export default reducerDishes;