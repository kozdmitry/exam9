import {
    FETCH_CONTACTS_FAILURE,
    FETCH_CONTACTS_REQUEST,
    FETCH_CONTACTS_SUCCESS,
    FETCH_INFO,
    POST_CONTACT
} from "./action";

const initialState = {
    loading: false,
    error: false,
    contacts: [],
    books:[],
    cont: []
};

const reducerContacts = (state = initialState, action) => {
    switch (action.type) {
        case POST_CONTACT:
            return {...state, books: action.books};
        case FETCH_CONTACTS_REQUEST:
            return {...state, loading: true, error: false};
        case FETCH_CONTACTS_SUCCESS:
            return {...state, books: action.books, loading: false};
        case FETCH_CONTACTS_FAILURE:
            return {...state, loading: false, error: true};
        case FETCH_INFO:
            return {...state, cont: action.cont, fetchLoading: false};
        default:
            return state;
    }
};

export default reducerContacts;