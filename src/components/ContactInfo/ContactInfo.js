import React from 'react';
import "./ContactInfo.css";

const ContactInfo = ({image, name, surname, phone}) => {
    return (
        <div>
            <p>{name}</p>
            <img className="img" src={image}/>
            <p>{surname}</p>
            <p>{phone}</p>
        </div>
    );
};

export default ContactInfo;