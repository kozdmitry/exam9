import React from 'react';
import {AppBar, Button, CssBaseline, Grid, Toolbar, Typography} from "@material-ui/core";
import {NavLink} from "react-router-dom";
import "./Layout.css";

const Layout = ({children}) => {
    return (
        <>
            <CssBaseline />
            <AppBar position="fixed">
                <Toolbar>
                    <Grid container justify="space-between">
                        <Grid item>
                            <Typography  variant="h6" noWrap>
                                <Button className="btn" component={NavLink} to="/" color="inherit">CONTACTS</Button>
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Button component={NavLink} to="/add" color="inherit">Add new contact</Button>
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
            <main>
                {children}
            </main>

        </>
    );
};

export default Layout;