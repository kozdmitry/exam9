import React from 'react';
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import {Button, makeStyles} from "@material-ui/core";
import {NavLink} from "react-router-dom";

const useStyle = makeStyles(() => ({
    root: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: '10px',
        marginBottom: '30px',
        marginTop: '20px',
    },
    contentBlock: {
        display: 'flex',
        width: '500px'
    },
    cover: {
        width: '150px',
        height: '150px',
        overflow: 'hidden',
        borderRadius: '4px'
    },
    content: {
        display: 'flex',
        flexDirection: 'column'
    },
    title: {
        margin: '20px, 50px',
        textDecoration: 'none'
    }
}));
const Contacts  = ({image, name, surname, id, del, on}) => {
    const classes = useStyle();

    return (
        <>
                <Card component={NavLink} to={"/edit/" + id} className={classes.root}>
                    <div className={classes.contentBlock}>
                        <CardMedia
                            className={classes.cover}
                            image={image}
                            title="Live from space album cover"
                        />
                        <CardContent className={classes.content}>
                            <Typography component="h6" variant="h6" className={classes.title}>
                                {name}
                            </Typography>
                            <Typography component="h6" variant="h6" color="textSecondary">
                                {surname}
                            </Typography>
                        </CardContent>
                    </div>
                    <div>
                        <Button onClick={del} variant="contained" color="primary">Del</Button>
                    </div>
                </Card>
        </>

    );
};

export default Contacts;