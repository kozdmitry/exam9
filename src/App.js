import React from "react";
import Layout from "./components/Layout/Layout";
import {Switch, Route} from "react-router-dom";
import addPage from "./container/addPage/addPage";
import editPage from "./container/editPage/editPage";
import contactsPage from "./container/contactsPage/contactsPage";


const App =() => (
    <Layout>
        <Switch>
            <Route path="/edit/:id" component={editPage} />
            <Route path="/add" component={addPage} />
            <Route path="/" exact component={contactsPage} />
        </Switch>
    </Layout>

);

export default App;