import React, {useEffect, useState} from 'react';
import {fetchInfoContact, putNewContact} from "../../store/action";
import {useDispatch, useSelector} from "react-redux";
import {Button, CssBaseline, Grid, TextField} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import "./editPage.css";

const EditPage = ({match, del}) => {
    const dispatch = useDispatch();
    const oneContact = useSelector(state => state.cont);

    console.log(oneContact);

    const [oneInfo, setOneInfo] = useState({
        name: '',
        surname: '',
        phone: '',
        email: '',
        image: '',
        id:''
    });

    useEffect(() => {
        dispatch(fetchInfoContact(match.params.id))
    }, [dispatch, match.params.id]);

    useEffect(() => {
        setOneInfo(oneContact)
    }, [oneContact])

    const putContactHandler = (e) => {
        e.preventDefault();
        dispatch(putNewContact(match.params.id, oneInfo));
    };


    const changeInfo = event => {
        const {name, value} = event.target;

        setOneInfo(prev =>({
            ...prev,
            [name]: value
        }));
    };

    // const delContact = (e, id) => {
    //     e.preventDefault();
    //     dispatch(deleteContact(id))
    // };

    console.log(oneInfo.id);

    return (
        <>
            <CssBaseline/>
                <Grid className="container">
                    <form onSubmit={putContactHandler} className="formAdd">
                        <Grid container direction="column" spacing={2}>
                            <Typography />
                            <Grid item xs={3} spacing={2}>
                                <Typography variant="h6">Add new Contacts</Typography>
                            </Grid>
                            <Grid item xs={6} spacing={2}>
                                <TextField
                                    placeholder="Name"
                                    name="name"
                                    spacing={3}
                                    variant="outlined"
                                    onChange={changeInfo}
                                    value={oneInfo.name}
                                >
                                </TextField>
                            </Grid>
                            <Grid item xs={6} spacing={2}>
                                <TextField
                                    placeholder="Surname"
                                    name="surname"
                                    variant="outlined"
                                    onChange={changeInfo}
                                    value={oneInfo.surname}
                                />
                            </Grid>
                            <Grid item xs={6} spacing={2}>
                                <TextField
                                    placeholder="Phone"
                                    name="phone"
                                    variant="outlined"
                                    onChange={changeInfo}
                                    value={oneInfo.phone}
                                />
                            </Grid>
                            <Grid item xs={6} spacing={2}>
                                <TextField
                                    placeholder="E-mail"
                                    name="email"
                                    variant="outlined"
                                    onChange={changeInfo}
                                    value={oneInfo.email}
                                />
                            </Grid>
                            <Grid item xs={6} spacing={2}>
                                <TextField
                                    placeholder="Img"
                                    name="image"
                                    variant="outlined"
                                    onChange={changeInfo}
                                    value={oneInfo.image}
                                />
                                <Grid item>
                                    <img className="imgAdd" src={oneInfo.image}/>
                                </Grid>
                            </Grid>
                            <Grid item>
                                <Button type="submit" variant="outlined" color="primary">Save</Button>
                                {/*<Button onClick={e => delContact(e, oneInfo.id)} type="submit" variant="outlined" color="primary">Del</Button>*/}
                            </Grid>
                        </Grid>
                    </form>
                </Grid>
         </>
    );
};

export default EditPage;