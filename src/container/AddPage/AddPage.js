import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {Button, CssBaseline, Grid, Paper, TextField, Typography} from "@material-ui/core";
import {newDishesAdd} from "../../store/action/actionDishes";
import "./AddPage.css";

const AddPage = (history) => {
    const dispatch = useDispatch();

    const [info, setInfo] = useState({
        name: '',
        price: '',
        image: ''
    })

    const post = (e) => {
        e.preventDefault();
        dispatch(newDishesAdd(info, history));
    };


    const changeInfo = event => {
        const {name, value} = event.target;

        setInfo(prev =>({
            ...prev,
            [name]: value
        }));
    };

    return (
        <>
            <CssBaseline/>
            <Paper className="container">
                <form onSubmit={post} className="formAdd">
                    <Grid container direction="column" spacing={2}>
                        <Typography />
                        <Grid item xs={3} spacing={2}>
                            <Paper spacing={3}>
                                <Typography variant="h6">Add new Dishes</Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={3}>
                            <Typography pb={5}>Menu</Typography>
                        </Grid>

                        <Grid item xs={6} spacing={2}>
                            <TextField
                                onChange={changeInfo}
                                fullWidth
                                name="name"
                                spacing={3}
                                variant="outlined"
                                value={info.name}
                            >
                            </TextField>
                        </Grid>
                        <Grid item xs={6} spacing={2}>
                            <TextField
                                placeholder="Price"
                                name="price"
                                variant="outlined"
                                onChange={changeInfo}
                                value={info.price}
                            />
                        </Grid><Grid item xs={6} spacing={2}>
                        <TextField
                            placeholder="Img"
                            name="image"
                            variant="outlined"
                            onChange={changeInfo}
                            value={info.image}
                        />
                    </Grid>
                        <Grid item>
                            <Button type="submit" variant="outlined" color="primary">Add</Button>
                        </Grid>
                    </Grid>
                </form>
            </Paper>
        </>

    );
};

export default AddPage;