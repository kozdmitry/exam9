import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {newContactAdd} from "../../store/action";
import {Button, CssBaseline, Grid, TextField, Typography} from "@material-ui/core";
import "./addPage.css";
import {NavLink} from "react-router-dom";

const AddPage = (history) => {

    const dispatch = useDispatch();

    const [info, setInfo] = useState({
        name: '',
        surname: '',
        phone: '',
        email: '',
        image: ''
    });

    const post = (e) => {
        e.preventDefault();
        dispatch(newContactAdd(info, history));
    };


    const changeInfo = event => {
        const {name, value} = event.target;

        setInfo(prev =>({
            ...prev,
            [name]: value
        }));
    };
    return (
        <>
            <CssBaseline/>
            <Grid className="container">
                <form onSubmit={post} className="formAdd">
                    <Grid container direction="column" spacing={2}>
                        <Typography />
                        <Grid item xs={3} spacing={2}>
                            <Typography variant="h6">Add new Contacts</Typography>
                        </Grid>
                        <Grid item xs={6} spacing={2}>
                            <TextField
                                placeholder="Name"
                                name="name"
                                spacing={3}
                                variant="outlined"
                                onChange={changeInfo}
                                value={info.name}
                            >
                            </TextField>
                        </Grid>
                        <Grid item xs={6} spacing={2}>
                            <TextField
                                placeholder="Surname"
                                name="surname"
                                variant="outlined"
                                onChange={changeInfo}
                                value={info.surname}
                            />
                        </Grid>
                        <Grid item xs={6} spacing={2}>
                            <TextField
                                placeholder="Phone"
                                name="phone"
                                variant="outlined"
                                onChange={changeInfo}
                                value={info.phone}
                            />
                        </Grid>
                        <Grid item xs={6} spacing={2}>
                            <TextField
                                placeholder="E-mail"
                                name="email"
                                variant="outlined"
                                onChange={changeInfo}
                                value={info.email}
                            />
                        </Grid>
                        <Grid item xs={6} spacing={2}>
                            <TextField
                                placeholder="Img"
                                name="image"
                                variant="outlined"
                                onChange={changeInfo}
                                value={info.image}
                            />
                            <Grid item>
                                <img className="imgAdd" src={info.image}/>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Button type="submit" variant="outlined" color="primary">Save</Button>
                            <Button component={NavLink} to="/" variant="outlined" color="primary">Back to Contacts</Button>
                        </Grid>
                    </Grid>
                </form>
            </Grid>
        </>
    );
};

export default AddPage;