import React, {useEffect} from 'react';
import {Grid} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {deleteContact, fetchContacts} from "../../store/action";
import Contacts from "../../components/Contacts/Contacts";
import "./contacsPage.css";

;

const ContactsPage = () => {
    const dispatch = useDispatch();
    const books = useSelector(state => state.books)

    useEffect(() => {
        dispatch(fetchContacts());
    }, [dispatch]);

    const delContact = (e, id) => {
        e.preventDefault();
        dispatch(deleteContact(id))
    };


    return (
        <>
            <Grid className="Contacts">
                <Grid container spacing={1}>
                    <Grid item>
                        {books.map(cont => (
                            <Contacts
                                key={cont.id}
                                id={cont.id}
                                name={cont.name}
                                surname={cont.surname}
                                image={cont.image}
                                del={e => delContact(e)}
                            />
                        ))}
                    </Grid>
                </Grid>
            </Grid>
        </>

    );
};

export default ContactsPage;
